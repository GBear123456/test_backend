const mongoose = require("mongoose");

//create a expam schema
const searchSchema = new mongoose.Schema({
  id:{
    type:Number,
    required: true,
  },
  year: {
    type: String,
    required: true,
  },
  status: {
    type: Number,
    required: true,
  },
  count: {
    type: Number,
    require: true,
  },
  quality: {
    type: String,
    required: true,
  }
});

module.exports = search = mongoose.model("Search", searchSchema);
