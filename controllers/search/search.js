const search = require('../../models/search/search');
const promise = require('../../middleware/promise');

/**
 * Get all the search
 * Paginate the search
 * Get the total number of search
 */
exports.getData = promise(async (req, res) => {
    const page = req.query.page;
    const ItemsPerPge = 10;
    const search = await search.find()
        .skip((page - 1) * ItemsPerPge)
        .limit(ItemsPerPge)
    const totalSearch = await search.countDocuments();
    res.json({ search, totalSearch });
});

/**
 * find search
 */
 exports.findData = promise(async (req, res) => {
     let query = {}
    if ( req.body.quality !== '' ){
        query.quality = req.body.quality
    } 
    if ( req.body.status != 0 ){
        query.status = req.body.status
    }

    const search = await search.find( query )
    res.json({ search });
});

